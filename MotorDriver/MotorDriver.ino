const int motorPWMPin = 6;
const int motorN1Pin = 7;
const int motorN2Pin = 8;

const int directionSwitchingIntervalControllerPin = A4;
const int speedControllerPin = A5;
const int buttonPin = 5;

const int redLEDPin = 9;
const int yellowLEDPin = 10;
const int greenLEDPin = 11;

const int indicatorLEDPin = 3;

class LEDIndicator {
private:
  const int ledPin;
  boolean currentState;

public:
  LEDIndicator(const int ledPin, const boolean currentState = LOW) 
    : ledPin(ledPin), currentState(currentState) {}

  void setState(const boolean state) {
    currentState = state;
    digitalWrite(ledPin, currentState);
  }
  
  boolean getCurrentState() {
    return currentState;
  }
};

class RandomModeManager {
private:
  const int buttonPin;
  const boolean isPullUp;

  const long debounce;
  long previousMillis;
  boolean previousActivation;
  
  boolean randomModeActive;
  
public:
  RandomModeManager(const int buttonPin, const boolean isPullUp = false, const int debounce = 200) 
    : buttonPin(buttonPin), isPullUp(isPullUp), debounce(debounce) {
    previousMillis = 0;
    previousActivation = true;
    randomModeActive = false;
  }

  void readButton() {
    const boolean buttonActivated = isPullUp ? !digitalRead(buttonPin) : digitalRead(buttonPin);
    
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= debounce) {
      if (buttonActivated && !previousActivation) {
        previousMillis = currentMillis;
        randomModeActive = !randomModeActive;
      }
    }
    
    previousActivation = buttonActivated;
  }
  
  boolean isRandomModeActive() {
    return randomModeActive;
  }
};

class SpeedLEDArray {
private:
  const int redLEDPin;
  const int yellowLEDPin;
  const int greenLEDPin;
  
public:
  SpeedLEDArray(const int redLEDPin, const int yellowLEDPin, const int greenLEDPin)
    : redLEDPin(redLEDPin), yellowLEDPin(yellowLEDPin), greenLEDPin(greenLEDPin) {}

  void displaySpeed(const short speedValue) {
    analogWrite(redLEDPin, constrain(speedValue - 170, 0, 255));
    analogWrite(yellowLEDPin, constrain((speedValue - 85) * 1.5, 0, 255));
    analogWrite(greenLEDPin, constrain(speedValue * 3, 0, 255));
  }
};

class Regulator {
private:
  const int potentiometerPin;
  
public:
  Regulator(const int potentiometerPin) : potentiometerPin(potentiometerPin) {}

  long read() {
    return analogRead(potentiometerPin);
  }
  
  short readPWM() {
    return map(read(), 0, 1023, 0, 255);
  }
};

class MotorController {
private:
  const int motorPWMPin; 
  const int motorN1Pin; 
  const int motorN2Pin;

  short motorSpeed;
  long directionSwitchingInterval;
  
  unsigned long previousMillis;
  boolean forward;
  
public:
  MotorController(const int motorPWMPin, const int motorN1Pin, const int motorN2Pin)
    : motorPWMPin(motorPWMPin), motorN1Pin(motorN1Pin), motorN2Pin(motorN2Pin) {
    previousMillis = 0;
    forward = false;
  }

  void setSpeed(const short speedValue) {
    motorSpeed = speedValue;
  }
  
  void setSwitchingInterval(const long switchingIntervalValue) {
    directionSwitchingInterval = switchingIntervalValue;
  }
  
  void run() {
    analogWrite(motorPWMPin, motorSpeed);
    
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= directionSwitchingInterval) {
      previousMillis = currentMillis;
  
      forward = !forward;
    
      digitalWrite(motorN1Pin, forward);
      digitalWrite(motorN2Pin, !forward);
    }
  }
};



LEDIndicator ledIndicator(indicatorLEDPin);
RandomModeManager randomModeManager(buttonPin, true);
  
SpeedLEDArray speedLEDArray(redLEDPin, yellowLEDPin, greenLEDPin);
Regulator directionSwitchingIntervalRegulator(directionSwitchingIntervalControllerPin);
Regulator speedRegulator(speedControllerPin);
  
MotorController motorController(motorPWMPin, motorN1Pin, motorN2Pin);

void setup() {
  pinMode(motorPWMPin, OUTPUT);
  pinMode(motorN1Pin, OUTPUT);
  pinMode(motorN2Pin, OUTPUT);
  
  pinMode(directionSwitchingIntervalControllerPin, INPUT);
  pinMode(speedControllerPin, INPUT);
  pinMode(buttonPin, INPUT);
  
  pinMode(redLEDPin, OUTPUT);
  pinMode(yellowLEDPin, OUTPUT);
  pinMode(greenLEDPin, OUTPUT);
  
  pinMode(indicatorLEDPin, OUTPUT);
  
  randomSeed(analogRead(A0));
}

void loop() {
  randomModeManager.readButton();
  const boolean randomModeActive = randomModeManager.isRandomModeActive();
  
  ledIndicator.setState(randomModeActive);
  
  short speedPWM = speedRegulator.readPWM();
  long directionSwitchingInterval = map(directionSwitchingIntervalRegulator.read(), 0, 1023, 0, 3000);
  
  directionSwitchingInterval = directionSwitchingInterval > 2700 ? 3600000 : directionSwitchingInterval;
  
  if (randomModeActive) {
    speedPWM = random(0, 255);
    directionSwitchingInterval = random(500, 2000);
  }
  
  speedLEDArray.displaySpeed(speedPWM);
  
  motorController.setSpeed(speedPWM);
  motorController.setSwitchingInterval(directionSwitchingInterval);
  motorController.run();
}




